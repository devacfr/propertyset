/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */
package com.opensymphony.module.propertyset.hibernate5;

import com.opensymphony.module.propertyset.PropertyException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class HibernatePropertySetDAOImpl implements HibernatePropertySetDAO
{
    private final SessionFactory sessionFactory;

    public HibernatePropertySetDAOImpl(final SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public void setImpl(final PropertySetItem item, final boolean isUpdate)
    {
        try
        {
            final Session session = sessionFactory.getCurrentSession();

            if (isUpdate)
            {
                session.update(item);
            }
            else
            {
                session.save(item);
            }

            session.flush();
        }
        catch (final HibernateException he)
        {
            throw new PropertyException("Could not save key '" + item.getKey() + "':" + he.getMessage());
        }
    }

    public Collection getKeys(final String entityName, final Long entityId, final String prefix, final int type)
    {
        final Session session = sessionFactory.getCurrentSession();
        List list;

        try
        {
            list = HibernatePropertySetDAOUtils.getKeysImpl(session, entityName, entityId, prefix, type);
        }
        catch (final HibernateException ignored)
        {
            list = Collections.emptyList();
        }
        finally
        {
            try
            {
                session.flush();
            }
            catch (final Exception ignored)
            {
            }
        }

        return list;
    }

    public PropertySetItem create(final String entityName, final long entityId, final String key)
    {
        return new PropertySetItemImpl(entityName, entityId, key);
    }

    public PropertySetItem findByKey(final String entityName, final Long entityId, final String key)
    {
        final PropertySetItem item;

        try
        {
            final Session session = sessionFactory.getCurrentSession();
            item = HibernatePropertySetDAOUtils.getItem(session, entityName, entityId, key);
            session.flush();
        }
        catch (final HibernateException ignored)
        {
            return null;
        }

        return item;
    }

    public void remove(final String entityName, final Long entityId)
    {
        try
        {
            final Session session = sessionFactory.getCurrentSession();

            //hani: todo this needs to be optimised rather badly, but I have no idea how
            final Collection<String> keys = getKeys(entityName, entityId, null, 0);

            for (final String key : keys)
            {
                session.delete(HibernatePropertySetDAOUtils.getItem(session, entityName, entityId, key));
            }

            session.flush();
        }
        catch (final HibernateException e)
        {
            throw new PropertyException("Could not remove all keys: " + e.getMessage());
        }
    }

    public void remove(final String entityName, final Long entityId, final String key)
    {
        try
        {
            final Session session = sessionFactory.getCurrentSession();
            session.delete(HibernatePropertySetDAOUtils.getItem(session, entityName, entityId, key));
            session.flush();
        }
        catch (final HibernateException e)
        {
            throw new PropertyException("Could not remove key '" + key + "': " + e.getMessage());
        }
    }
}
