/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */
package com.opensymphony.module.propertyset.ejb3;

import com.opensymphony.module.propertyset.AbstractPropertySetTest;
import com.opensymphony.module.propertyset.DatabaseHelper;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;

import java.util.HashMap;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/**
 * @author Hani Suleiman
 *         Date: Nov 8, 2005
 *         Time: 6:11:09 PM
 */
@Ignore("Not really going to bother about getting EJB tests started until we have a reason to use this impl.")
public class EJBPropertySetTest //extends AbstractPropertySetTest
{
    private PropertySet ps;
    //~ Static fields/initializers /////////////////////////////////////////////

    private static EntityManagerFactory factory;

    @BeforeClass
    public static void setupDatabase() throws Exception
    {
        DatabaseHelper.setupJNDIDataSource();
        DatabaseHelper.createDatabase("");

        Properties props = new Properties();
        props.put("hibernate.dialect", "org.hibernate.dialect.MckoiDialect");
        props.put("javax.persistence.transactionType", "RESOURCE_LOCAL");

        factory = Persistence.createEntityManagerFactory("propertyset", props);
    }

    //~ Methods ////////////////////////////////////////////////////////////////

    @Before
    public void setUp() throws Exception {
        HashMap args = new HashMap();
        args.put("entityName", "testejb3");
        args.put("entityId", new Long(3));

        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        args.put("manager", em);
        ps = PropertySetManager.getInstance("ejb3", args);
    }

    @After
    public void tearDown() throws Exception {
        ps.remove();
    }
}
