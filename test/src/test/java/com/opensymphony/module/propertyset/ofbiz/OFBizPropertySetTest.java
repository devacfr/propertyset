/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */
package com.opensymphony.module.propertyset.ofbiz;

import com.opensymphony.module.propertyset.AbstractPropertySetTest;
import org.junit.After;
import org.junit.Before;

import java.util.HashMap;


/**
 * User: bbulger
 * Date: May 24, 2004
 */
public class OFBizPropertySetTest extends AbstractPropertySetTest {
    //~ Methods ////////////////////////////////////////////////////////////////

    @Before
    public void setUp() throws Exception {
        // Give the JDBC driver a kick in the right direction
        Class.forName("org.hsqldb.jdbcDriver");

        ps = new OFBizPropertySet();

        HashMap args = new HashMap();
        args.put("entityId", new Long(1));
        args.put("entityName", "test");
        ps.init(new HashMap(), args);
    }

    @After
    public void tearDown() throws Exception {
        ps.remove();
    }
}
