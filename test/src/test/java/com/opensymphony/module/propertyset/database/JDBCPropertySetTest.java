/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */
package com.opensymphony.module.propertyset.database;

import com.opensymphony.module.propertyset.AbstractPropertySetTest;
import com.opensymphony.module.propertyset.DatabaseHelper;
import com.opensymphony.module.propertyset.PropertySetManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;

import java.util.HashMap;


/**
 * This test case attempts to verify the database implementation.  This is also a good resource for beginners
 * to PropertySet.  This leverages straight JDBC as the persistence mechanism which requires
 * fewer dependencies then hibernate..
 *
 * @author Eric Pugh (epugh@upstate.com)
 */
public class JDBCPropertySetTest extends AbstractPropertySetTest
{
    @BeforeClass
    public static void setupDatabase() throws Exception
    {
        DatabaseHelper.setupJNDIDataSource();
    }

    @After
    public void tearDown()
    {
        ps.remove();
    }

    @Before
    public void setUp() throws Exception
    {
        HashMap args = new HashMap();
        args.put("globalKey", "test");
        args.put("datasource", "jdbc/DefaultDS");
        ps = PropertySetManager.getInstance("jdbc", args);
    }
}
